#! /usr/bin/env python

def read(filename):
	db = []
	row = []

	for line in open(filename):
		line = line.strip()
		if line == '' and len(row) != 0:
			db.append(row)
			row = []
		else:
			if line[0] == '#':
				continue
			else:
				parts = line.split('\t')
				pair = (parts[2], parts[6], parts[7])
				row.append(pair)

	return db

if __name__ == "__main__":
	import sys
	import json

	if len(sys.argv) != 3:
		print("Usage: python %s <conllu file> <json file>" % sys.argv[0])
		exit()
	else:
		db = read(sys.argv[1])
		with open(sys.argv[2], 'w') as fout:
			json.dump(db, fout)
		print("conllu to json: %s -> %s" % (sys.argv[1], sys.argv[2]))
