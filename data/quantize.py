#! /usr/bin/env python

import json
from collections import Counter

word_counter = Counter()
relation_counter = Counter()
for line in open('relations.txt'):
	parts = line.split(':')
	if len(parts) == 2:
		relation_counter[parts[0]] = 0

maxlength = 0
db = None

filenames = {
	'en-ud-dev.json': 'en-ud-dev-quant.json',
	'en-ud-train.json': 'en-ud-train-quant.json',
}

for name in filenames.keys():
	with open(name) as fin:
		db = json.load(fin)

		for sentence in db:
			if len(sentence) > maxlength:
				maxlength = len(sentence)
			for word in sentence:
				word_counter[word[0]] += 1
				relation_counter[word[2]] += 1

word_map = {value[0]: index for index, value in enumerate(word_counter.most_common())}
relation_map = {value[0]: index for index, value in enumerate(relation_counter.most_common())}

print(maxlength)
print(len(word_map))
print(len(relation_map))

for name, nameq in filenames.items():
	with open(name) as fin:
		db = json.load(fin)
		db2 = []

		for sentence in db:
			row = []
			try:
				for word in sentence:
					pair = (word_map[word[0]], -int(word[1]), relation_map[word[2]])
					row.append(pair)
				db2.append(row)
			except Exception as e:
				print(e)

		with open(nameq, 'w') as fout:
			json.dump(db2, fout)
