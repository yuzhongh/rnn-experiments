import tensorflow as tf
from tensorflow.models.rnn.translate import data_utils
import rnn_cell
import random
import numpy as np

class RNNModel(object):
	def __init__(self, config, mode, cell_mode=None, no_previous=False):
		assert mode in ["train", "inference"]
		self.batch_size = config.batch_size
		self.cell_units = config.cell_units
		self.not_shared = False
		self.num_input_symbols = config.num_input_symbols
		self.num_output_symbols = config.num_output_symbols
		self.num_layers = config.num_layers
		self.max_input_seq_length = config.max_input_seq_length
		self.max_output_seq_length = config.max_output_seq_length
		self.embedding_size = config.embedding_size
		self.act_func = config.activation_func
		self.num_samples = config.num_samples
		self.max_gradient_norm = config.max_gradient_norm
		self.no_previous = no_previous
		self.max_checkpoints_to_keep = config.max_checkpoints_to_keep
		self.mode = mode
		self.mode = 'train'
		self.inputs = []
		self.targets = []
		self.weights = []
		self.initial_state = None


		self.global_step = tf.Variable(0, name="GlobalStep", trainable=False)
		self.learning_rate = tf.Variable(float(config.learning_rate), name="LearningRate", trainable=False)
		self.learning_rate_decay_op = self.learning_rate.assign(
			self.learning_rate * config.learning_rate_decay_factor)

		# data feed
		for i in xrange(self.max_input_seq_length):
			self.inputs.append(tf.placeholder(dtype=tf.int64,
	                                  shape=[None],  # batch_size
	                                  name="input_feed{0}".format(i)))
		for i in xrange(self.max_output_seq_length):
			self.targets.append(tf.placeholder(dtype=tf.int64,
	                                  shape=[None],  # batch_size
	                                  name="target_feed{0}".format(i)))
			self.weights.append(tf.placeholder(dtype=tf.float32,
	                                  shape=[None],  # batch_size
	                                  name="weights{0}".format(i)))
		
  		# cell definition
		#single_cell = RNN_cell.SRNCell(self.cell_units, activation=self.act_func)
		#single_cell = tf.nn.rnn_cell.BasicRNNCell(self.cell_units, activation=self.act_func)
		#single_cell = tf.nn.rnn_cell.LSTMCell(self.cell_units, state_is_tuple=False, activation=self.act_func)
		#single_cell = tf.nn.rnn_cell.GRUCell(self.cell_units, state_is_tuple=False, activation=self.act_func)
		#single_cell = RNN_cell.Modified_LSTMCell(self.cell_units, state_is_tuple=False, activation=self.act_func)
		'''
		if cell_mode:
			if cell_mode == 'LSTM':
				single_cell = RNN_cell.LSTMCell(self.cell_units, state_is_tuple=False, 
					no_previous=self.no_previous, activation=self.act_func)
			elif cell_mode == 'GRU':
				single_cell = tf.nn.rnn_cell.GRUCell(self.cell_units, activation=self.act_func)
			#elif cell_mode == 'BASIC':
			#	single_cell = tf.nn.rnn_cell.BasicRNNCell(self.cell_units, activation=self.act_func)
			#elif cell_mode == 'cLSTM':
			#	single_cell = RNN_cell.Modified_LSTMCell(cell_mode, self.cell_units, state_is_tuple=False, activation=self.act_func)
			elif cell_mode == 'ctLSTM':
				single_cell = RNN_cell.Modified_LSTMCell(self.cell_units, state_is_tuple=False, 
					no_previous=self.no_previous, activation=self.act_func)
				self.not_shared = True
			elif cell_mode == 'ctnfLSTM':
				single_cell = RNN_cell.Modified_LSTMCell(self.cell_units, state_is_tuple=False, 
					no_previous=self.no_previous, no_forget=True, activation=self.act_func)
				self.not_shared = True
		'''
		single_cell = rnn_cell.LSTMCellC(self.cell_units, state_is_tuple=False, activation=self.act_func)
		if self.num_layers > 1:
			self.cell = rnn_cell.MultiRNNCell([single_cell] * self.num_layers, state_is_tuple=False)
		else:
			self.cell = single_cell
		#import pdb
		#pdb.set_trace()
		self.zero_state = self.cell.zero_state(self.batch_size, tf.float32)
		self.initial_state = tf.placeholder(dtype=tf.float32,
									shape=self.zero_state.get_shape().as_list(),
									name="initial_state")

	def get_batch(self, data):
		batch_inputs, batch_targets = [],[]
		for batch_idx in xrange(self.batch_size):
			instance = random.choice(data)
			# instance is input, target pairs
			input_ids, output_ids = instance[0]
			# reverse input and target sequences
			#input_ids = list(reversed(input_ids))
			#output_ids = list(reversed(output_ids))
			# end of reverse
			input_ids = input_ids[:self.max_input_seq_length]
			output_ids += [data_utils.EOS_ID]
			if len(output_ids) > self.max_output_seq_length:
				output_ids = output_ids[:self.max_output_seq_length-1]+[data_utils.EOS_ID]
			
			# create batch
			pad = [data_utils.PAD_ID] * (len(self.inputs)-len(input_ids))
			batch_inputs.append(input_ids+pad)
			#if self.mode == 'train':
			batch_targets.append(output_ids+
					[data_utils.PAD_ID] * (len(self.targets)-len(output_ids)))

		#reindex batch inputs into sequence inputs
		inputs, targets, target_weights = [],[],[]
		for length_idx in xrange(len(self.inputs)):
			inputs.append(np.array([batch_inputs[batch_idx][length_idx]
								for batch_idx in xrange(self.batch_size)], dtype=np.int32))
		#if self.mode == 'train':
		if batch_targets:
			for length_idx in xrange(len(self.targets)):
				targets.append(np.array([batch_targets[batch_idx][length_idx]
					for batch_idx in xrange(self.batch_size)], dtype=np.int32))
				# We set weight to 0 if the corresponding target is a PAD symbol.
				target_weight = np.ones(self.batch_size, dtype=np.float32)
				for batch_idx in xrange(self.batch_size):
					if batch_targets[batch_idx][length_idx] == data_utils.PAD_ID:
						target_weight[batch_idx] = 0.0
				target_weights.append(target_weight)

		return inputs, targets, target_weights
