import tensorflow as tf
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.ops import variable_scope as vs
from tensorflow.python.ops import embedding_ops

import rnn_model
import rnn_cell
import seq2seq

def basic_rnn(inputs,
              cell,
              num_input_symbols,
              num_output_symbols,
              embedding_size,
              output_projection=None,
              feed_previous=False,
              initial_state=None,
              dtype=dtypes.float32,
              not_shared=False,
              scope=None):
  with vs.variable_scope(scope or "basic_rnn") as scope:
    if output_projection is not None:
      dtype = scope.dtype
      proj_weights = ops.convert_to_tensor(output_projection[0], dtype=dtype)
      proj_weights.get_shape().assert_is_compatible_with([None, num_output_symbols])
      proj_biases = ops.convert_to_tensor(output_projection[1], dtype=dtype)
      proj_biases.get_shape().assert_is_compatible_with([num_output_symbols])
    else:
      cell = rnn_cell.OutputProjectionWrapperC(cell, num_output_symbols)
    embedding = None
    loop_function = None
    emb_inp = inputs
    if embedding_size > 0:
      embedding = vs.get_variable("embedding",
                                              [num_input_symbols, embedding_size])
      if feed_previous:
        loop_function = seq2seq._extract_argmax_and_embed(
            embedding, output_projection,# feed previous only when inferencing
            update_embedding=True) 
      emb_inp = (embedding_ops.embedding_lookup(embedding, i)
                 for i in inputs)
  
    return seq2seq.rnn_decoder(
        emb_inp, initial_state, cell, loop_function=loop_function)

def make_model(encoder_inputs,
               targets,
               weights,
               Net,
               softmax_loss_function=None,
               initial_state=None,
               decoder_inputs=None,
               per_example_loss=False,
               name=None):
  if decoder_inputs:
    all_inputs = encoder_inputs + decoder_inputs + targets + weights
  else:
    all_inputs = encoder_inputs + targets + weights
  with ops.name_scope(name, "make_model", all_inputs):
    with vs.variable_scope(
        vs.get_variable_scope(), reuse=None):
      if decoder_inputs:
        outputs, state = Net(encoder_inputs,
                        decoder_inputs)
      elif initial_state is not None:
        outputs, state = Net(encoder_inputs, 
                        initial_state)
      else:
        outputs, state = Net(encoder_inputs)
      if per_example_loss:
        losses = seq2seq.sequence_loss_by_example(
                    outputs,
                    targets,
                    weights,
                    softmax_loss_function=softmax_loss_function)
      else:
        losses = seq2seq.sequence_loss(
                    outputs,
                    targets,
                    weights,
                    softmax_loss_function=softmax_loss_function)
  return outputs, losses, state

class BasicRNNModel(rnn_model.RNNModel):
	def __init__(self, config, mode, forward_only, feed_previous, cell_mode=None, no_previous=False):
		super(BasicRNNModel, self).__init__(config, mode, cell_mode=cell_mode, no_previous=no_previous)

		# make sampled softmax
		output_projection = None
		softmax_loss_function = None
		if self.num_samples and self.num_samples < self.num_output_symbols:
			w = tf.get_variable("proj_w", [self.cell_units, self.num_output_symbols])
			w_t = tf.transpose(w)
			b = tf.get_variable("proj_b", [self.num_output_symbols])
			output_projection = (w, b)
			def sampled_loss(labels, inputs):
				labels = tf.reshape(labels, [-1, 1])
				local_w_t = tf.cast(w_t, tf.float32)
				local_b = tf.cast(b, tf.float32)
				local_inputs = tf.cast(inputs, tf.float32)
				return tf.nn.sampled_softmax_loss(
					weights=local_w_t,
					biases=local_b,
					labels=labels,
					inputs=local_inputs,
					num_sampled=self.num_samples,
					num_classes=self.num_output_symbols)
			softmax_loss_function = sampled_loss

		# one to one learning task
		def Net(inputs,feed_previous,initial_state=None):
			return basic_rnn(
				inputs, self.cell,
				num_input_symbols=self.num_input_symbols,
				num_output_symbols=self.num_output_symbols,
				embedding_size=self.embedding_size,
				output_projection=output_projection,
				feed_previous=feed_previous,
				initial_state=initial_state,
				not_shared=self.not_shared)

		with vs.variable_scope('SRN_Model'):
			self.outputs, self.losses, self.state = make_model(
					self.inputs, self.targets, self.weights,
					lambda x,y: Net(x,
								feed_previous=feed_previous,
								initial_state=y),
					softmax_loss_function=softmax_loss_function,
					initial_state=self.initial_state)
			if forward_only:
				if output_projection is not None:
					for b,output in enumerate(self.outputs):
						self.outputs[b] = tf.matmul(output, output_projection[0]) + output_projection[1]

		# Gradients and SGD update operation for training the model.
		params = tf.trainable_variables()
		if not forward_only:
			self.gradient_norms = []
			self.updates = []
			#opt = tf.train.AdamOptimizer(self.learning_rate)
			opt = tf.train.GradientDescentOptimizer(self.learning_rate)
			gradients = tf.gradients(self.losses, params)
			clipped_gradients, norm = tf.clip_by_global_norm(gradients, 
													self.max_gradient_norm)
			self.gradient_norms.append(norm)
			self.updates.append(opt.apply_gradients( 
				zip(clipped_gradients, params), global_step=self.global_step))
		self.saver = tf.train.Saver(tf.global_variables(), max_to_keep=self.max_checkpoints_to_keep)

	def step(self, session, inputs, targets, weights, forward_only, initial_state=None):
		if len(self.inputs) != len(inputs):
			raise ValueError("self.inputs length and inputs length mismatch,"
						" %d != %d." % (len(self.inputs), len(inputs)))

		input_feed = {}
		for i in range(len(inputs)):
			input_feed[self.inputs[i].name] = inputs[i]
		for i in range(len(targets)):
			input_feed[self.targets[i].name] = targets[i]
		for i in range(len(weights)):
			input_feed[self.weights[i].name] = weights[i]
		if initial_state is not None:
			input_feed[self.initial_state.name] = initial_state
		else:
			input_feed[self.initial_state.name] = self.zero_state.eval()

		if not forward_only:
			output_feed = [self.updates[0],
							self.losses,
							self.state]
		else:
			output_feed = [self.outputs,
						   self.losses,
						   self.state]

		outputs = session.run(output_feed, input_feed)
		if not forward_only:
			return None, outputs[1], outputs[2]
		else:
			return outputs[0], outputs[1], outputs[2]
